//
//  ChatVCViewController.swift
//  QuickChat
//
//  Created by Amala on 08/06/17.
//  Copyright © 2017 Amala. All rights reserved.
//

import UIKit
import Photos
import CoreLocation

class ChatVCViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate,  UINavigationControllerDelegate {
    
    @IBOutlet var inputBar: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var inputTextField: UITextField!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    
    var typingIndicatorView:UIView!
    @IBOutlet var chatHeadeView: UIView!
    
    var conversationList : [(Bool,String)] = [(false,"How May I Help You?"),(true,"Hi, This is Amala"),(true,"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque dictum, ipsum id efficitur pulvinar, erat arcu eleifend massa, at hendrerit nulla nisi nec risus. Duis quis nunc massa. Proin non quam in ipsum rutrum ultricies vitae at urna. In eget sapien risus. Nullam lacinia ex vestibulum libero porttitor accumsan.")]
    
    var timeStamp = Date()
    
    let locationManager = CLLocationManager()
    var items = [Message]()
    //let imagePicker = UIImagePickerController()
    let barHeight: CGFloat = 50
    //var currentUser: User?
    //var canSendLocation = true
    
    override var inputAccessoryView: UIView? {
        get {
            self.inputBar.frame.size.height = self.barHeight
            self.inputBar.clipsToBounds = true
            return self.inputBar
        }
    }
    
    override var canBecomeFirstResponder: Bool{
        return true
    }
    
    func loadTypingIndicatorView(){
        typingIndicatorView = UIView(frame: CGRect(x: 0, y: (self.inputView?.frame.origin.y)!-10, width:self.view.frame.size.width, height: 10))
        self.view.addSubview(typingIndicatorView)
        typingIndicatorView.backgroundColor = UIColor.red
    }
    
    func loadChatHeaderView(){
        let view = (Bundle.main.loadNibNamed("ChatHeaderView", owner: self, options: nil)?[0] as? UIView)
        tableView.tableHeaderView = view
    }
    
    func customization(){
        self.tableView.estimatedRowHeight = self.barHeight
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.contentInset.bottom = self.barHeight
        self.tableView.scrollIndicatorInsets.bottom = self.barHeight

        self.navigationItem.title = "Helpdesk"
        self.navigationItem.setHidesBackButton(true, animated: false)
        let icon = UIImage.init(named: "back")?.withRenderingMode(.alwaysOriginal)
        let backButton = UIBarButtonItem.init(image: icon!, style: .plain, target: self, action: #selector(ChatVCViewController.dismissSelf))
        self.navigationItem.leftBarButtonItem = backButton
        
        let simulateReceiverButton = UIBarButtonItem(title: "Simulate", style: UIBarButtonItemStyle.plain, target: self, action: #selector(ChatVCViewController.simulateReceiverMessages))
        navigationItem.rightBarButtonItem = simulateReceiverButton
        
        //navBar.setItems([navItem], animated: false)
    }
    
    func loadInitialData(){
        createMessageWithText(text: "How May I Help You?", owner: .receiver)
        DispatchQueue.main.async {
            if self.items.count > 0 {
                self.tableView.reloadData()
                self.tableView.scrollToRow(at: IndexPath.init(row: self.items.count - 1, section: 0), at: .bottom, animated: false)
                
            }
        }
    }
    
    func createMessageWithText(text:String, owner:MessageOwner) {
        let message = Message.init(owner: owner, content: text, timeStamp: Int(Date().timeIntervalSince1970))
        //conversationList.append(isSender, text)
        items.append(message)
        tableView.beginUpdates()
        tableView.insertRows(at: [IndexPath(row: items.count-1, section: 0)], with: .automatic)
        tableView.endUpdates()
    }
    
    func dismissSelf(){
       print("Back Button clicked")
    }
    
    func simulateReceiverMessages(){
        createMessageWithText(text: "Thank you. Have a nice day!", owner: .receiver)
    }
    
    func animateExtraButtons(toHide: Bool)  {
        switch toHide {
        case true:
            self.bottomConstraint.constant = 0
            UIView.animate(withDuration: 0.3) {
                self.inputBar.layoutIfNeeded()
            }
        default:
            self.bottomConstraint.constant = -50
            UIView.animate(withDuration: 0.3) {
                self.inputBar.layoutIfNeeded()
            }
        }
    }
    
    @IBAction func showMessage(_ sender: Any) {
        self.animateExtraButtons(toHide: true)
    }
    
    @IBAction func showOptions(_ sender: Any) {
        self.animateExtraButtons(toHide: false)
    }
    
    @IBAction func sendMessage(_ sender: Any) {
        if let text = self.inputTextField.text {
            if text.characters.count > 0 {
                self.createMessageWithText(text: inputTextField.text!, owner: .sender)
                self.inputTextField.text = ""
            }
        }
    }
    
    //MARK: NotificationCenter handlers
    func showKeyboard(notification: Notification) {
        if let frame = notification.userInfo![UIKeyboardFrameEndUserInfoKey] as? NSValue {
            let height = frame.cgRectValue.height
            self.tableView.contentInset.bottom = height
            self.tableView.scrollIndicatorInsets.bottom = height
            if self.items.count > 0 {
                self.tableView.scrollToRow(at: IndexPath.init(row: self.items.count - 1, section: 0), at: .bottom, animated: true)
            }
        }
    }
    
    //MARK: Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if tableView.isDragging {
            cell.transform = CGAffineTransform.init(scaleX: 0.5, y: 0.5)
            UIView.animate(withDuration: 0.3, animations: {
                cell.transform = CGAffineTransform.identity
            })
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell  {
        switch self.items[indexPath.row].owner {
        case .sender:
            let cell = tableView.dequeueReusableCell(withIdentifier: "Receiver", for: indexPath) as! ReceiverCell
            cell.clearCellData()
            cell.message.text = self.items[indexPath.row].content
            return cell
        case .receiver:
            let cell = tableView.dequeueReusableCell(withIdentifier: "Sender", for: indexPath) as! SenderCell
            cell.clearCellData()
            cell.profilePic.image = UIImage(named: "educator")
            cell.message.text = self.items[indexPath.row].content
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return chatHeadeView
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        self.inputTextField.resignFirstResponder()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        print("User is typing..")
//        if typingIndicatorView.isHidden == true{
//            typingIndicatorView.isHidden = false
//        }
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.customization()
        //self.loadChatHeaderView()
        //self.loadTypingIndicatorView()
        //typingIndicatorView.isHidden = true
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: ViewController lifecycle
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.inputBar.backgroundColor = UIColor.clear
        self.view.layoutIfNeeded()
        NotificationCenter.default.addObserver(self, selector: #selector(ChatVCViewController.showKeyboard(notification:)), name: Notification.Name.UIKeyboardWillShow, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
