//
//  ChatViewController.swift
//  kidobotikz-web-ios-app
//
//  Created by Amala on 06/06/17.
//  Copyright © 2017 Amala. All rights reserved.
//

import UIKit

class ChatViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {

    private let cellId = "cellId"
    var chatMessages = [[String: AnyObject]]()
    var bottomConstraint:NSLayoutConstraint?

    
    // Bottom Layer - For inputing the text
    let messageInputContainerView:UIView = {
        let view = UIView()
        view.backgroundColor = .white
        return view
    }()
    
    let inputTextField:UITextField = {
        let tf = UITextField()
        tf.placeholder = "Enter Message Here"
        return tf
    }()
    
    /*let sendButton:UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Send", for: .normal)
        let titleColor = UIColor.returnRGBColor(r: 0, g: 137, b: 249, alpha: 1)
        button.setTitleColor(titleColor, for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        button.addTarget(self, action: #selector(handleSend), for: .touchUpInside)
        return button
    }()*/
    
    func handleSend(){
        print("Text entered:\(inputTextField.text)")
        if (inputTextField.text?.characters.count)! > 0 {
            SocketIOManager.sharedInstance.sendMessage(message: inputTextField.text!, isSender: true)
        }

    }
    
    private func setupInputComponents(){
        let topBorderView = UIView()
        topBorderView.backgroundColor = UIColor(white: 0.5, alpha: 0.5)
        
        // Send Button
        let sendButton:UIButton = UIButton(type: UIButtonType.system)
        sendButton.setTitle("Send", for: .normal)
        let titleColor = UIColor.returnRGBColor(r: 0, g: 137, b: 249, alpha: 1)
        sendButton.setTitleColor(titleColor, for: UIControlState.normal)
        sendButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        sendButton.addTarget(self, action: #selector(handleSend), for: UIControlEvents.touchUpInside)
        
        messageInputContainerView.addSubview(inputTextField)
        messageInputContainerView.addSubview(sendButton)
        messageInputContainerView.addSubview(topBorderView)
        
        messageInputContainerView.addConstraintsWithFormat(format: "H:|-8-[v0][v1(60)]|", views: inputTextField ,sendButton)
        messageInputContainerView.addConstraintsWithFormat(format: "V:|[v0]|", views: inputTextField)
        messageInputContainerView.addConstraintsWithFormat(format: "V:|[v0]|", views: sendButton)
        messageInputContainerView.addConstraintsWithFormat(format: "H:|[v0]|", views: topBorderView)
        messageInputContainerView.addConstraintsWithFormat(format: "V:|[v0(0.5)]", views: topBorderView)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadNavigationBar()
        self.view.backgroundColor = UIColor.white
        collectionView?.backgroundColor = .white
        self.collectionView?.isScrollEnabled = true
        self.collectionView?.alwaysBounceVertical = true
        self.collectionView!.register(ChatlogCell.self, forCellWithReuseIdentifier: cellId)
        view.addSubview(messageInputContainerView)
        view.addConstraintsWithFormat(format: "H:|[v0]|", views: messageInputContainerView)
        view.addConstraintsWithFormat(format: "V:[v0(48)]", views: messageInputContainerView)
        bottomConstraint = NSLayoutConstraint(item: messageInputContainerView, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1, constant: 0)
        view.addConstraint(bottomConstraint!)
        setupInputComponents()
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboard), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboard), name: NSNotification.Name.UIKeyboardWillHide, object: nil)

        // Do any additional setup after loading the view.
    }
    
    func handleKeyboard(notification:NSNotification){
        if let userInfo = notification.userInfo {
             let keyboardFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as AnyObject).cgRectValue
             let keyboardShowing = notification.name == NSNotification.Name.UIKeyboardWillShow
            bottomConstraint?.constant = keyboardShowing ? -(keyboardFrame?.height)! : 0
            print("Keyboard frame:\(keyboardFrame?.height)")
            UIView.animate(withDuration: 0, delay: 0, options: .curveEaseOut, animations: {
                self.view.layoutIfNeeded()
            }, completion: { (completed) in
                if (keyboardShowing){
                    if self.chatMessages.count > 0 {
                        let lastItem = self.chatMessages.count - 1
                        let indexPath = IndexPath(item: lastItem, section: 0)
                        self.collectionView?.scrollToItem(at: indexPath, at: .bottom, animated: true)
                    }
                    
                }
            })

        }
    }
    
    func loadNavigationBar(){
        let backButton = UIBarButtonItem(title: "Back", style: UIBarButtonItemStyle.plain, target: self, action: #selector(ChatController.closeChatView))
        navigationItem.leftBarButtonItem = backButton
        
        let simulateReceiverButton = UIBarButtonItem(title: "Simulate", style: UIBarButtonItemStyle.plain, target: self, action: #selector(ChatController.simulateReceiverMessages))
        navigationItem.rightBarButtonItem = simulateReceiverButton
        
        navigationItem.title = "Chat with us!"
    }

    func closeChatView(){
        dismiss(animated: true, completion: nil)
    }
    
    func simulateReceiverMessages(){
        SocketIOManager.sharedInstance.sendMessage(message: "Happy Roboting! How may we help you?", isSender: false)
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
          let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! ChatlogCell
        let currentChatMessage = chatMessages[indexPath.item]
        let message = currentChatMessage["message"] as! String
        let isSender = currentChatMessage["isSender"] as! Bool
        let messageDate = currentChatMessage["date"] as! String
        
        cell.messageTextView.text = message
        
        let size = CGSize(width: 250, height: 1000)
        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        let estimatedFrame = NSString(string: message).boundingRect(with: size, options: options, attributes: [NSFontAttributeName:UIFont.systemFont(ofSize: 18)], context: nil)
        if isSender == false{
            cell.profileImageView.isHidden = false
            cell.profileImageView.image = UIImage(named: "educator")
            cell.messageTextView.frame = CGRect(x: 48+8, y: 0, width: estimatedFrame.width + 16, height: estimatedFrame.height+20)
            cell.textBubbleView.frame = CGRect(x: 48 - 10, y: -4, width: estimatedFrame.width + 16 + 8 + 16, height: estimatedFrame.height+20+6)
            
            cell.bubbleImageView.image = ChatlogCell.grayBubbleImage
            cell.bubbleImageView.tintColor = UIColor(white: 0.95, alpha: 1)
            cell.messageTextView.textColor = UIColor.black
        }
        else{
            //outgoing sender messages
            cell.profileImageView.isHidden = true
            cell.messageTextView.frame = CGRect(x: view.frame.width - estimatedFrame.width - 16 - 16 - 8 , y: 0, width: estimatedFrame.width + 16, height: estimatedFrame.height+20)
            cell.textBubbleView.frame = CGRect(x: view.frame.width-estimatedFrame.width - 16 - 8 - 16 - 10, y: -4, width: estimatedFrame.width + 16 + 8 + 10, height: estimatedFrame.height+20+6)
            
            cell.bubbleImageView.image = ChatlogCell.blueBubbleImage
            cell.bubbleImageView.tintColor = UIColor.returnRGBColor(r: 0, g: 137, b: 249, alpha: 1)
            cell.messageTextView.textColor = UIColor.white
        }
        cell.timeLabel.textColor = UIColor.gray
        cell.timeLabel.text = messageDate
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        inputTextField.endEditing(true)
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return chatMessages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let currentChatMessage = chatMessages[indexPath.item]
        let message = currentChatMessage["message"] as! String
        if message != "" {
            let size = CGSize(width: 250, height: 1000)
            let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
            let estimatedFrame = NSString(string: message).boundingRect(with: size, options: options, attributes: [NSFontAttributeName:UIFont.systemFont(ofSize: 18)], context: nil)
            return CGSize(width: view.frame.width, height: estimatedFrame.height+20)
        }
        return CGSize(width: view.frame.width, height: 100)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(8, 0, 0, 0)
    }
    
    override func viewDidAppear(_ animated: Bool){
        super.viewDidAppear(animated)
        
        SocketIOManager.sharedInstance.getChatMessage(completionHandler: { (messageInfo) -> Void in
            DispatchQueue.global(qos: .background).async {
                self.chatMessages.append(messageInfo)
                DispatchQueue.main.async {
                    
                    let item = self.chatMessages.count - 1
                    let insertionIndexPath = NSIndexPath(item: item, section: 0)
                    if self.chatMessages.count == 1{
                        self.collectionView?.reloadData()
                    } else{
                        self.collectionView?.insertItems(at: [insertionIndexPath as IndexPath])
                    }
                    self.collectionView?.scrollToItem(at: insertionIndexPath as IndexPath, at: .bottom, animated: true)
                    self.inputTextField.text = nil
                }
                
            }
        })
        
        /*SocketIOManager.sharedInstance.getChatMessage(completionHandler: { (messageInfo) -> Void in
            DispatchQueue.main.async {
                self.chatMessages.append(messageInfo)
            }
        })*/
        
        /*SocketIOManager.sharedInstance.getChatMessage(completionHandler: { (messageInfo) -> Void in
            DispatchQueue.main.async {
                self.chatMessages.append(messageInfo)
                let item = self.chatMessages.count - 1
                let insertionIndexPath = NSIndexPath(item: item, section: 0)
                self.collectionView?.insertItems(at: [insertionIndexPath as IndexPath])
                self.collectionView?.scrollToItem(at: insertionIndexPath as IndexPath, at: .bottom, animated: true)
                self.inputTextField.text = nil
            }
        })*/
    }
    
    override func viewWillAppear(_ animated: Bool) {
        /*SocketIOManager.sharedInstance.getChatMessage(completionHandler: { (messageInfo) -> Void in
            DispatchQueue.global(qos: .background).async {
                self.chatMessages.append(messageInfo)
                DispatchQueue.main.async {
                    
                    let item = self.chatMessages.count - 1
                    let insertionIndexPath = NSIndexPath(item: item, section: 0)
                    if self.chatMessages.count == 1{
                        self.collectionView?.reloadData()
                    } else{
                        self.collectionView?.insertItems(at: [insertionIndexPath as IndexPath])
                    }
                    self.collectionView?.scrollToItem(at: insertionIndexPath as IndexPath, at: .bottom, animated: true)
                    self.inputTextField.text = nil
                }

            }
        })*/
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
