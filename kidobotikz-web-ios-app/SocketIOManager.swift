//
//  SocketIOManager.swift
//  kidobotikz-web-ios-app
//
//  Created by Amala on 06/06/17.
//  Copyright © 2017 Amala. All rights reserved.
//

import UIKit

class SocketIOManager: NSObject {
    
    static let sharedInstance = SocketIOManager()
    
    var socket: SocketIOClient = SocketIOClient(socketURL: NSURL(string: "http://192.168.1.131:3000") as! URL)
    
    override init(){
        super.init()
    }
    
    func establishConnection(){
        socket.connect()
    }
    
    func closeConnection(){
        socket.disconnect()
    }
    
    func connectToServerWithNickname(nickname:String,completionHandler:@escaping (_ userList:[[String:AnyObject]]?) -> Void){
        socket.emit("connectUser",nickname)
        
        socket.on("userList"){(dataArray,ack) -> Void in
            completionHandler((dataArray[0] as! [[String:AnyObject]]))
        }
    }
    
    func exitChatWithNickname(nickname:String,completionHandler:() -> Void){
        socket.emit("exitUser",nickname)
        completionHandler()
    }
    
    func sendMessage(message:String,isSender:Bool){
        socket.emit("chatMessage",message,isSender)
    }
    
    func getChatMessage(completionHandler:@escaping (_ messageInfo:[String:AnyObject])->Void){
        socket.on("newChatMessage"){(dataArray,socketAck) -> Void in
            var messageDictionary = [String:AnyObject]()
            messageDictionary["message"] = dataArray[0] as! String as AnyObject?
            messageDictionary["isSender"] = dataArray[1] as! Bool as AnyObject?
            messageDictionary["date"] = dataArray[2] as! String as AnyObject?

            completionHandler(messageDictionary)
            
        }
    }
}
