//
//  KDBWebViewController.swift
//  kidobotikz-web-ios-app
//
//  Created by Amala on 31/05/17.
//  Copyright © 2017 Amala. All rights reserved.
//

import UIKit
import WebKit

class KDBWebViewController: UIViewController,WKNavigationDelegate, FloatyDelegate, WKScriptMessageHandler {

    @IBOutlet var containerView: UIView! = nil

    var webView : WKWebView!
    
    var currentUrl : String!
    
    var username:String!
    
    let userContentController = WKUserContentController()
    
    override func loadView() {
        super.loadView()
        
        /*let config = WKWebViewConfiguration()
        let scriptURL:String = Bundle.main.path(forResource: "hideFreshDeskChatWidget", ofType: "js")!
        let scriptContent = try? String(contentsOfFile: scriptURL, encoding: String.Encoding.utf8)
        
        let script = WKUserScript(source: scriptContent!, injectionTime: .atDocumentStart, forMainFrameOnly: true)
        config.userContentController.addUserScript(script)
        self.webView = WKWebView(frame: CGRect.zero, configuration: config)*/
    
        let config = WKWebViewConfiguration()
        config.userContentController = userContentController
        self.webView = WKWebView(frame: self.view.bounds, configuration: config)
        userContentController.add(self, name: "userLogin")

        
        //self.webView = WKWebView()
        self.view = self.webView!
        webView.navigationDelegate = self
        //webView.addSubview(headerViewForChat)
        
    }
    
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        let dict = message.body as! [String:AnyObject]
        let username    = dict["username"] as! String
        print("USERNAME:\(username)")
        // now use the name as you see fit!
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadKidobotikzWebPage()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func loadKidobotikzWebPage(){
        let kidoUrl = NSURL(string: "http://kidobotikz.dev/")
        let request = NSURLRequest(url: kidoUrl as! URL)
        self.webView!.load(request as URLRequest)
    }
    
    func loadFloatingChatButton(){
        /*let item = FloatyItem()
        item.buttonColor = UIColor.blue
        item.circleShadowColor = UIColor.red
        item.titleShadowColor = UIColor.blue
        item.title = "Chat with us!"
        item.handler = { item in
            
        }
        
        floaty.addItem("Chat with us", icon: UIImage(named: "educator"))
        floaty.addItem(item: item)
        floaty.paddingX = self.webView.frame.width/2 - floaty.frame.width/2
        floaty.fabDelegate = self*/
        
        //Floaty.global.button.addItem(title: "Chat with us")
        //Floaty.global.show()
        
        let floaty = Floaty()
        floaty.addItem("Helpdesk", icon: UIImage(named:"chat"), handler: {item in
            
            /*let layout = UICollectionViewFlowLayout()
            let chatViewController = ChatController(collectionViewLayout: layout)
            
            //chatViewController.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Back", style: UIBarButtonItemStyle.plain, target: self, action: #selector(KDBWebViewController.closeChatView))
            
            //chatViewController.navigationItem.title = "Chat with us!"
            
            let chatControllerNavigation = UINavigationController(rootViewController: chatViewController)
            //self.navigationController?.pushViewController(chatControllerNavigation, animated: true)
            self.present(chatControllerNavigation, animated: true, completion: nil)*/
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "Chat")
            let chatControllerNavigation = UINavigationController(rootViewController: controller)
            self.present(chatControllerNavigation, animated: true, completion: nil)

        })
        
        floaty.addItem("Video Chat", icon: UIImage(named:"video_chat"), handler: {item in
            let videoChatViewController = VideoChatViewController()
            let videoChatControllerNavigation = UINavigationController(rootViewController: videoChatViewController)
            //self.navigationController?.pushViewController(chatControllerNavigation, animated: true)
            self.present(videoChatControllerNavigation, animated: true, completion: nil)
            
        })
        
        self.webView.addSubview(floaty)
    }
    
    
    func floatyOpened(_ floaty: Floaty) {
        print("Floaty Opened")
    }
    
    func floatyClosed(_ floaty: Floaty) {
        print("Floaty Closed")
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("Page Finished Navigation")
        currentUrl = webView.url?.absoluteString as String!
        print("Current URL:\(currentUrl)")
        if currentUrl.contains("user"){
            self.loadFloatingChatButton()
        }
        self.loadFloatingChatButton()
        
    }
    
    /*func closeChatView(){
        dismiss(animated: true, completion: nil)
    }*/
}
