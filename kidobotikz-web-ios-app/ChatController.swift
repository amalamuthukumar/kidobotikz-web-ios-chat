//
//  ChatController.swift
//  kidobotikz-web-ios-app
//
//  Created by Adithyha Jayakumar on 03/06/17.
//  Copyright © 2017 Amala. All rights reserved.
//

import UIKit
import OpenTok

private let reuseIdentifier = "Cell"

class ChatController: UICollectionViewController,UICollectionViewDelegateFlowLayout, OTSessionDelegate, UITextFieldDelegate {

    var conversationList : [(Bool,String)] = [(false,"How May I Help You?"),(true,"Hi, This is Amala"),(true,"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque dictum, ipsum id efficitur pulvinar, erat arcu eleifend massa, at hendrerit nulla nisi nec risus. Duis quis nunc massa. Proin non quam in ipsum rutrum ultricies vitae at urna. In eget sapien risus. Nullam lacinia ex vestibulum libero porttitor accumsan.")]
    
    var timeStamp = Date()
    
    var users = [[String: AnyObject]]()
    var nickname: String!
    var configurationOk = false
    var chatMessages = [[String: AnyObject]]()
    
    var session: OTSession?
    
    var kApiKey = "45880782"
    
    var kSessionId = "1_MX40NTg4MDc4Mn5-MTQ5NjgzMDgzOTg3N35EWlB1STAyWTIrV0VNRTR2VnNEY255Q2x-fg"
    
    var kToken = "T1==cGFydG5lcl9pZD00NTg4MDc4MiZzZGtfdmVyc2lvbj1kZWJ1Z2dlciZzaWc9M2IyNDgzOTNkZDA5ZmJlOTk3N2Y4MzZjMmQ5ZTUyZGNiYTRhMmQ2NjpzZXNzaW9uX2lkPTFfTVg0ME5UZzRNRGM0TW41LU1UUTVOamd6TURnek9UZzNOMzVFV2xCMVNUQXlXVElyVjBWTlJUUjJWbk5FWTI1NVEyeC1mZyZjcmVhdGVfdGltZT0xNDk2ODMwODM5JnJvbGU9cHVibGlzaGVyJm5vbmNlPTE0OTY4MzA4MzkuOTA0MjEzMjkzODAyODYmZXhwaXJlX3RpbWU9MTQ5OTQyMjgzOQ=="

    
    let messageInputContainerView:UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        return view
    }()
    
    let inputTextField:UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Enter Message Here"
        return textfield
    }()
    
    /*let sendButton:UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Send", for: .normal)
        let titleColor = UIColor.returnRGBColor(r: 0, g: 137, b: 249, alpha: 1)
        button.setTitleColor(titleColor, for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        button.addTarget(self, action: #selector(handleSend), for: .touchUpInside)
        return button
    }()*/


   
    
    var blockOperations = [BlockOperation]()
    
    func handleSend() {
        print("Text entered:\(inputTextField.text)")
        
        createMessageWithText(text: inputTextField.text!, isSender: true)
        
        inputTextField.text = ""
        
        //self.collectionView?.insertItems(at: newIndexPath as! Int)
    }
    
    private func setupInputComponents(){
        let topBorderView = UIView()
        topBorderView.backgroundColor = UIColor(white: 0.5, alpha: 0.5)
        
        // Send Button
        let sendButton:UIButton = UIButton(type: UIButtonType.system)
        sendButton.setTitle("Send", for: .normal)
        let titleColor = UIColor.returnRGBColor(r: 0, g: 137, b: 249, alpha: 1)
        sendButton.setTitleColor(titleColor, for: UIControlState.normal)
        sendButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        sendButton.addTarget(self, action: #selector(sendChatMessage), for: UIControlEvents.touchUpInside)
        
        messageInputContainerView.addSubview(inputTextField)
        messageInputContainerView.addSubview(sendButton)
        messageInputContainerView.addSubview(topBorderView)
        
        messageInputContainerView.addConstraintsWithFormat(format: "H:|-8-[v0][v1(60)]|", views: inputTextField ,sendButton)
        messageInputContainerView.addConstraintsWithFormat(format: "V:|[v0]|", views: inputTextField)
        messageInputContainerView.addConstraintsWithFormat(format: "V:|[v0]|", views: sendButton)
        messageInputContainerView.addConstraintsWithFormat(format: "H:|[v0]|", views: topBorderView)
        messageInputContainerView.addConstraintsWithFormat(format: "V:|[v0(0.5)]", views: topBorderView)
    }
    
     var bottomConstraint:NSLayoutConstraint?
    
    
     override func viewDidLoad() {
        super.viewDidLoad()

        let backButton = UIBarButtonItem(title: "Back", style: UIBarButtonItemStyle.plain, target: self, action: #selector(ChatController.closeChatView))
        navigationItem.leftBarButtonItem = backButton
        
        let simulateReceiverButton = UIBarButtonItem(title: "Simulate", style: UIBarButtonItemStyle.plain, target: self, action: #selector(ChatController.simulateReceiverMessages))
        navigationItem.rightBarButtonItem = simulateReceiverButton
        
        navigationItem.title = "Chat with us!"
        
        self.view.backgroundColor = UIColor.white
        collectionView?.backgroundColor = .white
        collectionView?.alwaysBounceVertical = true
        collectionView?.isScrollEnabled = true
        
        // Register cell classes
        self.collectionView!.register(ChatlogCell.self, forCellWithReuseIdentifier: reuseIdentifier)
        
        /*self.collectionView!.register(ChatController.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "ChatHeaderView")

        if let flowLayout = self.collectionView?.collectionViewLayout as? UICollectionViewFlowLayout
        {
            flowLayout.headerReferenceSize = CGSize(width: (self.collectionView?.frame.size.width)!, height: 250)
        }*/

        view.addSubview(messageInputContainerView)
        
        view.addConstraintsWithFormat(format: "H:|[v0]|", views: messageInputContainerView)
        view.addConstraintsWithFormat(format: "V:[v0(48)]", views: messageInputContainerView)
        bottomConstraint = NSLayoutConstraint(item: messageInputContainerView, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1, constant: 0)
        view.addConstraint(bottomConstraint!)
        
        setupInputComponents()
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboard), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboard), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        self.inputTextField.delegate = self
        
        self.doConnect()

    }
    
    
    
    func handleKeyboardDidShowNotification(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            if let keyboardFrame = (userInfo[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
                bottomConstraint?.constant = keyboardFrame.size.height
                view.layoutIfNeeded()
            }
        }
    }
    
    
    func handleKeyboardDidHideNotification(notification: NSNotification) {
        bottomConstraint?.constant = 0
        view.layoutIfNeeded()
    }
    
    func handleKeyboard(notification:NSNotification){
        if let userInfo = notification.userInfo{
            let keyboardFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as AnyObject).cgRectValue
            let isKeyboardShowing = notification.name == NSNotification.Name.UIKeyboardWillShow
            bottomConstraint?.constant = isKeyboardShowing ? -(keyboardFrame?.height)! : 0
            UIView.animate(withDuration: 0, delay: 0, options: .curveEaseOut, animations: {
                self.view.layoutIfNeeded()
            }, completion: { (completed) in
                if (isKeyboardShowing){
                    let lastItem = (self.conversationList.count) - 1
                    let indexPath = IndexPath(item: lastItem, section: 0)
                    self.collectionView?.scrollToItem(at: indexPath, at: .bottom, animated: true)
                }
            })
        }
    }
    
    func createMessageWithText(text:String, isSender:Bool=false){
        
        conversationList.append((isSender,text))
        
        print("Conversation List count:\(conversationList.count)")
        
        let indexPath = IndexPath(item: conversationList.count-1, section: 0)
        blockOperations.append(BlockOperation(block: {
            (self.collectionView?.insertItems(at: [indexPath]))!
            
        }))
        self.collectionView?.reloadData()
    }
    
    func simulateReceiverMessages(){
        createMessageWithText(text: "Please wait for a moment", isSender: false)
    }
    
    func closeChatView(){
        dismiss(animated: true, completion: nil)
    }
    
    /*func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        let headerSize = CGSize(width: (self.collectionView?.frame.width)!, height: 44)
        return headerSize
    }*/
    
    /*override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let headerView: ChatHeaderView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "ChatHeaderView", for: indexPath) as! ChatHeaderView
        headerView.frame.size.width = (self.collectionView?.frame.width)!
        headerView.frame.size.height = 100
        headerView.facultyNameLbl.text = "Faculty"
        headerView.facultyStatusLbl.text = "Online"
        return headerView
    }*/
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! ChatlogCell
        
        let (isSender,message) = conversationList[indexPath.item]
        
        /*let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mm a"
        let elapsedTimeInSeconds = NSDate().timeIntervalSince(timeStamp as Date)
        let secondsInDay : TimeInterval = 60*60*24
        
        if(elapsedTimeInSeconds > 7*secondsInDay){
            dateFormatter.dateFormat = "MM/dd/yy"
        } else if elapsedTimeInSeconds > secondsInDay{
            dateFormatter.dateFormat = "EEE"
        }*/
        
        //let ts = dateFormatter.string(from: timeStamp as Date)
        
        /*let timeStampAttributes = [NSForegroundColorAttributeName: UIColor.gray, NSFontAttributeName: UIFont.systemFont(ofSize: 14)]
        
        let ts = NSMutableAttributedString(string: dateFormatter.string(from: timeStamp as Date), attributes: timeStampAttributes)

        let msgAttr = [NSFontAttributeName: UIFont.systemFont(ofSize: 18)]
        
        let msg = NSMutableAttributedString(string: message, attributes: msgAttr)
        let nl = NSMutableAttributedString(string: "\n\n", attributes: nil)

        
        let messageWithTimestamp = NSMutableAttributedString()
        
        messageWithTimestamp.append(msg)
        messageWithTimestamp.append(nl)
        messageWithTimestamp.append(ts)*/
        
        //let messageWithTimestamp = message + "\n" + ts
        
        cell.messageTextView.text = message
        
        if message != "" {
            
            print("Inside message")
            let size = CGSize(width: 250, height: 1000)
            let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
            let estimatedFrame = NSString(string: message).boundingRect(with: size, options: options, attributes: [NSFontAttributeName:UIFont.systemFont(ofSize: 18)], context: nil)
            
            if isSender == false{
                cell.profileImageView.isHidden = false
                cell.profileImageView.image = UIImage(named: "educator")
                cell.messageTextView.frame = CGRect(x: 48+8, y: 0, width: estimatedFrame.width + 16, height: estimatedFrame.height+20)
                cell.textBubbleView.frame = CGRect(x: 48 - 10, y: -4, width: estimatedFrame.width + 16 + 8 + 16, height: estimatedFrame.height+20+6)
                
                cell.bubbleImageView.image = ChatlogCell.grayBubbleImage
                cell.bubbleImageView.tintColor = UIColor(white: 0.95, alpha: 1)
                cell.messageTextView.textColor = UIColor.black
                
            }
            else{
                
                //outgoing sender messages
                cell.profileImageView.isHidden = true
                cell.messageTextView.frame = CGRect(x: view.frame.width - estimatedFrame.width - 16 - 16 - 8 , y: 0, width: estimatedFrame.width + 16, height: estimatedFrame.height+20)
                cell.textBubbleView.frame = CGRect(x: view.frame.width-estimatedFrame.width - 16 - 8 - 16 - 10, y: -4, width: estimatedFrame.width + 16 + 8 + 10, height: estimatedFrame.height+20+6)
                
                cell.bubbleImageView.image = ChatlogCell.blueBubbleImage
                cell.bubbleImageView.tintColor = UIColor.returnRGBColor(r: 0, g: 137, b: 249, alpha: 1)
                cell.messageTextView.textColor = UIColor.white
            }
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "hh:mm a"
            let elapsedTimeInSeconds = NSDate().timeIntervalSince(timeStamp as Date)
            let secondsInDay : TimeInterval = 60*60*24
            
            if(elapsedTimeInSeconds > 7*secondsInDay){
                dateFormatter.dateFormat = "MM/dd/yy"
            } else if elapsedTimeInSeconds > secondsInDay{
                dateFormatter.dateFormat = "EEE"
            }
            cell.timeLabel.textColor = UIColor.gray
            cell.timeLabel.text = dateFormatter.string(from: timeStamp as Date)
        }
        
        return cell
    }

    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        inputTextField.endEditing(true)
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return conversationList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let (_,message) = conversationList[indexPath.item]
        
        if message != "" {
            let size = CGSize(width: 250, height: 1000)
            let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
            let estimatedFrame = NSString(string: message).boundingRect(with: size, options: options, attributes: [NSFontAttributeName:UIFont.systemFont(ofSize: 18)], context: nil)
            return CGSize(width: view.frame.width, height: estimatedFrame.height+20)
        }
        return CGSize(width: view.frame.width, height: 100)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(8, 0, 0, 0)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
   /* override func viewDidAppear(_ animated: Bool) {
        let section:NSInteger = (collectionView?.numberOfSections)! - 1
        let lastItem:NSInteger = (collectionView?.numberOfItems(inSection: section))! - 1
        let lastIndexPath = IndexPath(item: lastItem, section: section)
        collectionView?.scrollToItem(at: lastIndexPath, at: .top, animated: true)
    }*/
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.layoutIfNeeded()
        let section:NSInteger = (collectionView?.numberOfSections)! - 1
        let lastItem:NSInteger = (collectionView?.numberOfItems(inSection: section))! - 1
        let lastIndexPath = IndexPath(item: lastItem, section: section)
        collectionView?.scrollToItem(at: lastIndexPath, at: .centeredVertically, animated: true)
    }
    
    func sendChatMessage(){
       
        session?.signal(withType: "chat", string: inputTextField.text, connection: nil, error: nil)
        //createMessageWithText(text: inputTextField.text!, isSender: true)
        inputTextField.text = ""
    }
    
    func doConnect(){
        self.session = OTSession(apiKey:kApiKey, sessionId:kSessionId, delegate:self)
        self.session?.connect(withToken: kToken, error: nil)
    }
    
    func session(_ session: OTSession, receivedSignalType type: String?, from connection: OTConnection?, with string: String?) {
        var isSender:Bool = false
        if connection?.connectionId == session.connection?.connectionId {
            isSender = true
        }
        self.createMessageWithText(text: string!, isSender: isSender)
    }
    
    func sessionDidConnect(_ session: OTSession) {
        print("The client connected to the OpenTok session.")
    }
    
    //If the client fails to connect to the OpenTok session, an OTError object is passed into the session(_: didFailWithError:) method.
    func sessionDidDisconnect(_ session: OTSession) {
        print("The client disconnected from the OpenTok session.")
    }
    
    //When the client disconnects from the OpenTok session, the sessionDidConnect(_:) method is called.
    func session(_ session: OTSession, didFailWithError error: OTError) {
        print("The client failed to connect to the OpenTok session: \(error).")
    }
    
    //When another client publishes a stream to the OpenTok session.
    func session(_ session: OTSession, streamCreated stream: OTStream) {
        print("A stream was created in the session.")
    }
    
    //When another client stops publishing a stream to the OpenTok session.
    func session(_ session: OTSession, streamDestroyed stream: OTStream) {
        print("A stream was destroyed in the session.")
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        inputTextField.text = ""
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.sendChatMessage()
        self.view.endEditing(true)
        return true
    }
    
  }

class ChatlogCell:BaseCell{
    
    static let grayBubbleImage = UIImage(named:"bubble_gray")?.resizableImage(withCapInsets: UIEdgeInsetsMake(22, 26, 22, 26)).withRenderingMode(.alwaysTemplate)
    static let blueBubbleImage = UIImage(named:"bubble_blue")?.resizableImage(withCapInsets: UIEdgeInsetsMake(22, 26, 22, 26)).withRenderingMode(.alwaysTemplate)
    
    
    let messageTextView : UITextView = {
        let textView = UITextView()
        textView.text = "Sample Message"
        textView.font = UIFont.systemFont(ofSize: 18)
        textView.backgroundColor = UIColor.clear
        textView.isEditable = false
        return textView
    }()
    
    let textBubbleView:UIView = {
        let view = UIView()
        view.layer.cornerRadius = 15
        view.layer.masksToBounds = true
        return view
    }()
    
    let bubbleImageView : UIImageView = {
        let iv = UIImageView()
        iv.image = ChatlogCell.grayBubbleImage
        iv.tintColor = UIColor(white: 0.90, alpha: 1)
        return iv
    }()
    
    let timeLabel : UILabel = {
        let label = UILabel()
        label.text = "12:00 pm"
        label.textColor = UIColor.gray
        label.font = UIFont.systemFont(ofSize: 12)
        return label
    }()
    
    let profileImageView: UIImageView = {
        let iv = UIImageView()
        iv.layer.cornerRadius = 15
        iv.layer.masksToBounds = true
        return iv
    }()
    
    override func setUpViews() {
        
        super.setUpViews()
        
        addSubview(textBubbleView)
        addSubview(messageTextView)
        addSubview(timeLabel)
        
        addSubview(profileImageView)
        addConstraintsWithFormat(format: "H:|-8-[v0(30)]|", views: profileImageView)
        addConstraintsWithFormat(format: "V:[v0(30)]|", views: profileImageView)
        
        textBubbleView.addSubview(bubbleImageView)
        textBubbleView.addConstraintsWithFormat(format: "H:|[v0]|", views: bubbleImageView)
        textBubbleView.addConstraintsWithFormat(format: "V:|[v0]|", views: bubbleImageView)
        
        addConstraintsWithFormat(format: "H:|-10-[v0]|", views: timeLabel)
        addConstraintsWithFormat(format: "V:[v0]-10-[v1]", views: bubbleImageView,timeLabel)
        
    }
    
}

class BaseCell:UICollectionViewCell {
    
    override init(frame:CGRect){
        super.init(frame: frame)
        setUpViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setUpViews(){
        
    }
}

extension UIView{
    func addConstraintsWithFormat(format:String, views: UIView...){
        
        var viewsDictionary = [String:UIView]()
        for (index, view) in views.enumerated(){
            let key = "v\(index)"
            view.translatesAutoresizingMaskIntoConstraints = false
            viewsDictionary[key] = view
        }
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: format, options: NSLayoutFormatOptions(), metrics: nil, views: viewsDictionary))
    }
    
}

extension UIColor{
    static func returnRGBColor(r:CGFloat, g:CGFloat, b:CGFloat, alpha:CGFloat) -> UIColor{
        return UIColor(red: r/255, green: g/255, blue: b/255, alpha: alpha)
    }
}


