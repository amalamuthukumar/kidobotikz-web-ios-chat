//
//  Message.swift
//  QuickChat
//
//  Created by Amala on 08/06/17.
//  Copyright © 2017 Amala. All rights reserved.
//

import Foundation
import UIKit

class Message {
    var owner: MessageOwner
    var content: String
    var timeStamp: Int
    
    init(owner: MessageOwner, content: String, timeStamp: Int) {
        self.owner = owner
        self.content = content
        self.timeStamp = timeStamp
    }
}
