//
//  ChatHeaderView.swift
//  kidobotikz-web-ios-app
//
//  Created by Amala on 07/06/17.
//  Copyright © 2017 Amala. All rights reserved.
//

import UIKit

class ChatHeaderView: UIView {

    @IBOutlet weak var closeChatBtn: UIButton!
    @IBOutlet weak var minimizeChatBtn: UIButton!
    @IBOutlet weak var videoChatBtn: UIButton!
    @IBOutlet weak var facultyStatusLbl: UILabel!
    @IBOutlet weak var facultyNameLbl: UILabel!
    @IBOutlet weak var facultyImageView: UIImageView!
    @IBOutlet weak var chatHeaderView: UIView!
    
    
    override init(frame:CGRect){
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder:NSCoder) {
        super.init(coder:aDecoder)
        commonInit()
    }
    
    func commonInit(){
        Bundle.main.loadNibNamed("ChatHeaderView", owner: self, options: [:])
        addSubview(chatHeaderView)
        chatHeaderView.translatesAutoresizingMaskIntoConstraints = false
        chatHeaderView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        chatHeaderView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        chatHeaderView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        chatHeaderView.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
