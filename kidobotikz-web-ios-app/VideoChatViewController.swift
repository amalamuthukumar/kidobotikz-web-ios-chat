//
//  VideoChatViewController.swift
//  kidobotikz-web-ios-app
//
//  Created by Amala on 05/06/17.
//  Copyright © 2017 Amala. All rights reserved.
//

import UIKit
import OpenTok

class VideoChatViewController: UIViewController,OTSessionDelegate,OTPublisherDelegate,OTSubscriberDelegate {

    /*@IBOutlet weak var subscriberView: UIView!
    @IBOutlet weak var publisherView: UIView!
    @IBOutlet weak var videoView: UIView!
    
    @IBOutlet weak var swapCameraBtn: UIButton!
    @IBOutlet weak var publisherAudioBtn: UIButton!
    @IBOutlet weak var subscriberAudioBtn: UIButton!*/
    
    var kApiKey = "45880782"
    
    var kSessionId = "2_MX40NTg4MDc4Mn5-MTQ5NjY2MzkyNDAxMX5uczhYUlFaUVNTS0RXWlM5am55TWhkdGh-fg"
    
    var kToken = "T1==cGFydG5lcl9pZD00NTg4MDc4MiZzZGtfdmVyc2lvbj1kZWJ1Z2dlciZzaWc9NjJhYTJkNGQ3MGQyYzlmNjVhZmFhOWViODRiZWIxOTllNjQ5Y2Q0ODpzZXNzaW9uX2lkPTJfTVg0ME5UZzRNRGM0TW41LU1UUTVOalkyTXpreU5EQXhNWDV1Y3poWVVsRmFVVk5UUzBSWFdsTTVhbTU1VFdoa2RHaC1mZyZjcmVhdGVfdGltZT0xNDk2NjYzOTI0JnJvbGU9cHVibGlzaGVyJm5vbmNlPTE0OTY2NjM5MjQuMDQ2NTI5NjY0NTEmZXhwaXJlX3RpbWU9MTQ5OTI1NTkyNA=="
    
    var session: OTSession?
    var publisher: OTPublisher?
    var subscriber: OTSubscriber?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let backButton = UIBarButtonItem(title: "Back", style: UIBarButtonItemStyle.plain, target: self, action: #selector(ChatController.closeChatView))
        navigationItem.leftBarButtonItem = backButton
        navigationItem.title = "Chat with us!"
        
        //self.connectToAnOpenTokSession()
        
        self.session = OTSession(apiKey:kApiKey, sessionId:kSessionId, delegate:self)
        self.session?.connect(withToken: kToken, error: nil)
    }

    func sessionDidConnect(_ session: OTSession) {
        
        self.publisher = OTPublisher(delegate:self)
        self.session?.publish(self.publisher!, error: nil)
        
        self.publisher?.view?.frame = CGRect(x: 10, y: 20, width: 170, height: 170)
        self.view.addSubview((self.publisher?.view)!)
    }
    
    func session(_ session: OTSession, streamCreated stream: OTStream) {
        
        self.subscriber = OTSubscriber(stream: stream, delegate:self)
        self.session?.subscribe(self.subscriber!, error: nil)
    }
    
    func subscriberVideoDataReceived(_ subscriber: OTSubscriber) {
        //subscriber.view?.frame = CGRect(x: 10, y: 20, width: 170, height: 170)
        subscriber.view?.frame = UIScreen.main.bounds
        self.view.addSubview(subscriber.view!)
        self.view?.bringSubview(toFront: (publisher?.view)!)
    }
    
    func sessionDidDisconnect(_ session: OTSession) {
        print("Session Disconnected")
    }
    
    func session(_ session: OTSession, streamDestroyed stream: OTStream) {
        print(stream.streamId)
    }
    
    func session(_ session: OTSession, didFailWithError error: OTError) {
        print(error)
    }
    
    public func publisher(_ publisher: OTPublisherKit, didFailWithError error: OTError) {
        print(error)
    }
    
    func subscriberDidConnect(toStream subscriber: OTSubscriberKit) {
        print(subscriber.stream?.connection.connectionId ?? "NO")
    }
    
    func subscriber(_ subscriber: OTSubscriberKit, didFailWithError error: OTError) {
        print(subscriber.stream?.streamId ?? "NO", error)
    }
    
    
    /*func connectToAnOpenTokSession() {
        session = OTSession(apiKey: kApiKey, sessionId: kSessionId, delegate: self)
        var error: OTError?
        session?.connect(withToken: kToken, error: &error)
        if error != nil {
            print(error!)
        }
    }*/
    
    func closeChatView(){
        dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*func loadPublisherView(){
       publisher?.view?.frame = CGRect(x: 0, y: 0, width: publisherView.bounds.size.width, height: publisherView.bounds.size.height)
        publisherView.addSubview((publisher?.view)!)
        
        publisherAudioBtn.isHidden = false
        publisherAudioBtn.addTarget(self, action: #selector(VideoChatViewController.togglePublisherMic), for: UIControlEvents.touchUpInside)
        
        swapCameraBtn.isHidden = false
        swapCameraBtn.addTarget(self, action: #selector(VideoChatViewController.swapCamera), for: UIControlEvents.touchUpInside)
    }
    func loadSubscriberView(){
        subscriber?.view?.frame = CGRect(x: 0, y: 0, width: subscriberView.bounds.size.width, height: subscriberView.bounds.size.height)
        subscriberView.addSubview((subscriber?.view)!)
        
        subscriberAudioBtn.isHidden = false
        subscriberAudioBtn.addTarget(self, action: #selector(VideoChatViewController.toggleSubscriberAudio), for: UIControlEvents.touchUpInside)
    }
    
    func togglePublisherMic(){
        var buttonImage = UIImage()
        publisher?.publishAudio = !(publisher?.publishAudio)!
        if (publisher?.publishAudio)! {
           buttonImage = UIImage(named: "mic-24.png")!
        } else{
            buttonImage = UIImage(named: "mic_muted-24.png")!
        }
        publisherAudioBtn.setImage(buttonImage, for: UIControlState.normal)
    }
    
    func toggleSubscriberAudio(){
        var buttonImage = UIImage()
        subscriber?.subscribeToAudio = !(subscriber?.subscribeToAudio)!
        if (subscriber?.subscribeToAudio)! {
            buttonImage = UIImage(named: "Subscriber-Speaker-35.png")!
        } else{
            buttonImage = UIImage(named: "Subscriber-Speaker-Mute-35.png")!
        }
        subscriberAudioBtn.setImage(buttonImage, for: UIControlState.normal)

    }
    
    func swapCamera(){
        if publisher?.cameraPosition == AVCaptureDevicePosition.front {
            publisher?.cameraPosition = AVCaptureDevicePosition.back
        } else{
            publisher?.cameraPosition = AVCaptureDevicePosition.front
        }
    }
    
    func cleanUpPublisher(){
        publisher?.view?.removeFromSuperview()
        publisher = nil
    }
    
    func cleanUpSubscriber(){
        subscriber?.view?.removeFromSuperview()
        subscriber = nil
    }*/
}

/*extension VideoChatViewController : OTSessionDelegate {
    
    //When the client connects to the OpenTok session, the sessionDidConnect(_:) method is called.
    func sessionDidConnect(_ session: OTSession) {
        print("The client connected to the OpenTok session.")
        
        let settings = OTPublisherSettings()
        settings.name = UIDevice.current.name
        guard let publisher = OTPublisher(delegate: self, settings: settings) else {
            return
        }
        
        var error: OTError?
        session.publish(publisher, error: &error)
        guard error == nil else {
            print(error!)
            return
        }
        
        //self.loadPublisherView()
        
        /*publisher.view?.frame = CGRect(x: 0, y: 0, width: publisherView.bounds.size.width, height: publisherView.bounds.size.height)
        publisherView.addSubview((publisher.view)!)
        
        publisherAudioBtn.isHidden = false
        publisherAudioBtn.addTarget(self, action: #selector(VideoChatViewController.togglePublisherMic), for: UIControlEvents.touchUpInside)
        
        swapCameraBtn.isHidden = false
        swapCameraBtn.addTarget(self, action: #selector(VideoChatViewController.swapCamera), for: UIControlEvents.touchUpInside)*/
        
        
        guard let publisherView = publisher.view else {
            return
        }
        let screenBounds = UIScreen.main.bounds
        publisherView.frame = CGRect(x: screenBounds.width - 150 - 20, y: screenBounds.height - 150 - 20, width: 150, height: 150)
        view.addSubview(publisherView)
        
    }
    
    //If the client fails to connect to the OpenTok session, an OTError object is passed into the session(_: didFailWithError:) method.
    func sessionDidDisconnect(_ session: OTSession) {
        print("The client disconnected from the OpenTok session.")
    }
    
    //When the client disconnects from the OpenTok session, the sessionDidConnect(_:) method is called.
    func session(_ session: OTSession, didFailWithError error: OTError) {
        print("The client failed to connect to the OpenTok session: \(error).")
    }
    
    //When another client publishes a stream to the OpenTok session.
    func session(_ session: OTSession, streamCreated stream: OTStream) {
        print("A stream was created in the session.")
        subscriber = OTSubscriber(stream: stream, delegate: self)
        guard let subscriber = subscriber else {
            return
        }
        
        var error: OTError?
        session.subscribe(subscriber, error: &error)
        guard error == nil else {
            print(error!)
            return
        }
        
        guard let subscriberView = subscriber.view else {
            return
        }
        subscriberView.frame = UIScreen.main.bounds
        view.insertSubview(subscriberView, at: 0)

        
        //self.loadSubscriberView()
        
        /*subscriber.view?.frame = CGRect(x: 0, y: 0, width: subscriberView.bounds.size.width, height: subscriberView.bounds.size.height)
        subscriberView.addSubview((subscriber.view)!)
        
        subscriberAudioBtn.isHidden = false
        subscriberAudioBtn.addTarget(self, action: #selector(VideoChatViewController.toggleSubscriberAudio), for: UIControlEvents.touchUpInside)
        
        self.view.addSubview(subscriberView)*/
        
        
    }
    
    //When another client stops publishing a stream to the OpenTok session.
    func session(_ session: OTSession, streamDestroyed stream: OTStream) {
        print("A stream was destroyed in the session.")
    }
}

extension VideoChatViewController: OTPublisherDelegate {
    func publisher(_ publisher: OTPublisherKit, didFailWithError error: OTError) {
        print("The publisher failed: \(error)")
        //self.cleanUpPublisher()
    }
    func publisher(_ publisher: OTPublisherKit, streamCreated stream: OTStream) {
        print("Now publishing")
    }
    func publisher(_ publisher: OTPublisherKit, streamDestroyed stream: OTStream) {
        //self.cleanUpPublisher()
    }
}

extension VideoChatViewController: OTSubscriberDelegate {
    public func subscriberDidConnect(toStream subscriber: OTSubscriberKit) {
        print("The subscriber did connect to the stream.")
        //loadSubscriberView()
    }
    
    public func subscriber(_ subscriber: OTSubscriberKit, didFailWithError error: OTError) {
        print("The subscriber failed to connect to the stream.")
    }
}*/

