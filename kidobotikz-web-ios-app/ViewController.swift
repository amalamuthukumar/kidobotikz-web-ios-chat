//
//  ViewController.swift
//  kidobotikz-web-ios-app
//
//  Created by Amala on 18/05/17.
//  Copyright © 2017 Amala. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var chatView: UIView!
 
    
    private lazy var chatViewController : ChatController = {
        let layout = UICollectionViewFlowLayout()
        let chatController = ChatController(collectionViewLayout: layout)
        self.add(asChildViewController: chatController)
        return chatController
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addSubview(chatView)
        setupView()
        
        //chatView.addSubview(chatViewController.view)

        
        /*addChildViewController(chatViewController)
        chatViewController.view.frame = self.chatView.frame
        
        self.chatView.addSubview(chatViewController.view)
        chatViewController.didMove(toParentViewController: self)*/
        
        /*chatView.addSubview(chatViewController.view)
        chatViewController.view.frame = self.chatView.bounds
        
        addChildViewController(chatViewController)
        chatViewController.didMove(toParentViewController: self)*/
        
        //self.addChildViewController(chatViewController)
        //self.chatView.addSubview(chatViewController.view)

        
    }
    
    func setupView(){
        add(asChildViewController: chatViewController)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
   
    private func add(asChildViewController collectionViewController:UICollectionViewController){
        addChildViewController(collectionViewController)
        self.chatView.addSubview(collectionViewController.view)
        collectionViewController.view.frame = chatView.bounds
        collectionViewController.view.autoresizingMask = [.flexibleWidth , .flexibleHeight]
        collectionViewController.didMove(toParentViewController: self)
    }
    

}

