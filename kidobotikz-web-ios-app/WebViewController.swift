//
//  WebViewController.swift
//  kidobotikz-web-ios-app
//
//  Created by Amala on 31/05/17.
//  Copyright © 2017 Amala. All rights reserved.
//

import UIKit
import WebKit

class WebViewController: UIViewController, WKUIDelegate, WKNavigationDelegate {

    var webView : WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        //Create WKWebView here,
        webView = WKWebView()
        
        webView.uiDelegate = self
        webView.navigationDelegate = self
        
        webView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(webView)
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "|-[webView]-|",
                                                                           options: NSLayoutFormatOptions(rawValue: 0),
                                                                           metrics: nil,
                                                                           views: ["webView": webView]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-20-[webView]-|",
                                                                           options: NSLayoutFormatOptions(rawValue: 0),
                                                                           metrics: nil,
                                                                           views: ["webView": webView]))
        
        let webViewKeyPathsToObserve = ["loading","estimatedProgress"]
        for keyPath in webViewKeyPathsToObserve {
            webView.addObserver(self, forKeyPath: keyPath, options: .new, context: nil)
        }
        
        self.loadKidobotikzWebPage()

    }
    
    func loadKidobotikzWebPage(){
        let kidoUrlString = "https://kidobotikz.com"
        guard let url = NSURL(string: kidoUrlString) else {return}
        let request = NSMutableURLRequest(url: url as URL)
        webView.load(request as URLRequest)
    }

    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        guard let keyPath = keyPath else {return}
        switch keyPath {
        
        case "loading":
                print("Loading Pages")
        case "estimatedProgress":
                print("Progress bar")
        
        default:
            break
        }
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("Page Finished Navigation")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @nonobjc func webView(webView: WKWebView, didFailNavigation navigation: WKNavigation!, withError error: NSError){
        handleError(error: error as NSError)
    }
    
    @nonobjc func webView(webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: NSError) {
        handleError(error: error as NSError)
    }
    
    func handleError(error: NSError) {
        if let failingUrl = error.userInfo["NSErrorFailingURLStringKey"] as? String {
            if let url = NSURL(string: failingUrl) {
                let didOpen = UIApplication.shared.openURL(url as URL)
                if didOpen {
                    print("openURL succeeded")
                    return
                }
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
